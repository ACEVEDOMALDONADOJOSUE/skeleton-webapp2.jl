/*
    Functions for converting a JSON object to an HTML table.

    The JSON object has the format:
    {
        "columns" => [name1, name2, ...]
        "name1"   => [df11, df21, ...]
        "namek"   => [df1k, df2k, ...]
    }
*/
    function json_to_pretty_table(data, elid) {
        json_to_html_table(data, elid);
        elid_expanded = "#" + elid;
        $(elid_expanded).DataTable();
    }

    function json_to_html_table(data, elid) {
        // Contruct basic HTML table from json data of the form:
        // data = {"columns": [colname1, colname2, ...],
        //         "colname1": [coldata1], "colname2": [coldata2], ...}
        var table = document.getElementById(elid);

        // Construct header
        var header = table.createTHead();
        var row = header.insertRow(0);
        var nj = data.columns.length;
        for(j = 0; j < nj; j++) {
            var th = document.createElement('th');
            th.innerHTML = data.columns[j];
            row.appendChild(th);
        }

        // Construct body
        var body = table.appendChild(document.createElement('tbody'));
        var ni = data[data.columns[0]].length;
        for(i = 0; i < ni; i++) {
            var row = body.insertRow(i);
            for(j = 0; j < nj; j++) {
                var cell = row.insertCell(j);
                var colname = data.columns[j];
                cell.innerHTML = data[colname][i];
            }
        }
    }
