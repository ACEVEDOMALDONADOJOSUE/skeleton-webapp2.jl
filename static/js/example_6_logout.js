// Contents: logout() function for Example 6

function logout() {
    var xhr;    // Request object
    if(window.XMLHttpRequest) {
	xhr = new XMLHttpRequest;
    } else {
	xhr = new ActiveXObject("Microsoft.XMLHTTP");    // IE6, IE5.
    }
    xhr.onreadystatechange = function() {    // Define callback function called as xhr.readyState progresses from 0 to 4.
	if(xhr.readyState == 4) {            // Request finshed, response is ready. 
	    // Cookie is revoked by server...redirect location is contained in xhr.responseText
	    if(window.location.pathname == "/home") {
		document.getElementById("welcome_text").innerHTML = "Welcome";
		document.getElementById("logout").className = "disabled";
	    } else{
	        window.location.replace(xhr.responseText);
	    }
	}
    };
    xhr.open("GET", "/logout", true);        // Define request and its destination
    xhr.send();
}
