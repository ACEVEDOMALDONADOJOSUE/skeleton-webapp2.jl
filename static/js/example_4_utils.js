// Contents: Utilities


function set_defaults() {
    // This function is called on page load
    var welcome_text = ""
    if(document.cookie.length == 0) {
        welcome_text = "Welcome...you";
    } else {
        var username = get_cookie("username")[0];
        welcome_text = "Welcome " + username;
    }
    document.getElementById("welcome_text").innerHTML = welcome_text;    // Set the welcome message
}


function get_cookie(cookie_name) {
    // Returns array of values for cookie_name
    result = [];
    var parts = document.cookie.split(cookie_name + "=");
    var nparts = parts.length;
    if(nparts == 1) {
        result.push("");
    } else {
        for(var i = 1; i < nparts; i++) {
            var value = parts[i].split(";")[0];
            result.push(value);
        }
    }
    return result;
}
