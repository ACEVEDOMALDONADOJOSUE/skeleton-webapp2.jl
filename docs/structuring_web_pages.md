# Structuring web pages

Below is an outline of the structure of the HTML templates used in the examples.

**HTML, CSS and Javascript**

From a language point of view the 3 main ingredients are HTML, CSS and Javascript. To understand what these languages do let's use an analogy: consider a *Word* document. It has content which is split into parts such as headers, paragraphs and so on. Each part of the content has styling. For example, headers may be bold, dark blue and 24pt, while paragraph text may be black and 12pt with a different font. In web pages, HTML specifies the content. Each part is defined by an *element*, which usually takes the form ```<element_name>element content goes here</element_name>```. For example, a header can be ```<h>My Header</h>```, and a paragraph can be ```<p>Paragraph text here</p>```. CSS does the stying. It tells the rendering engine where elements are placed and what they look like (fonts, size, colour, etc). The rendering engine is a program that is bundled with your browser that converts the html/css/javascript document that you write into the web page image that you see.

Web pages can also have interactive features like tool tips, zooming and sliders. These features are typically bound to specific elements (via the ```id``` attribute) or groups of elements (via the ```class``` attribute) using Javascript. Moreover, their behaviour is also encoded in Javascript. In fact the content itself can be completely specified with Javascript. You can see this on the *Model* page in Example 3 (and several other places) - the div element with id *chart* has no content until the script at the bottom of the page populates the element with chart content and defines tool tips when data points are hovered over, among other behaviours. The content of the chart is specified in the handler, as is the binding of the content to the element with ```id=chart```. The behaviour of the chart is defined in the c3.min.js script that is also included at the bottom of the page.

**Document Structure**

The pages (HTML documents) in this repo adhere to the following structure:

- DOC
- html
- head
    - meta data
        - character encoding
        - author
        - other stuff
    - title
    - styling (inline and/or linked css files)
- body
    - Navigation: A *navbar*, the strip across the top of the page containing links
    - Content: text, charts, maps, tables, dashboard, etc.
    - Scripts: included last so that the content loads first, otherwise the scripts have nothing to operate on.
- html

Optionally, you can add a footer after the body.

**Bootstrap**

[Bootstrap](http://getbootstrap.com/javascript) is a framework developed at Twitter for structuring HTML documents. Essentially it is a CSS file and a Javascript file that respectively encode a set of styles and behaviours for HTML elements. Bootstrap is easy to use, well documented and enables consistent page styling across devices. Here we consider only the content section in the body of our documents. From there the other sections are easily understood with the help of the [Bootstrap](http://getbootstrap.com/javascript) documentation.

The main rules for page content that is consistent across devices are:

1. Have a single container element, namely ```<div class="container"></div>``` or ```<div class="container-fluid"></div>```. Try both to see the difference. Do not have more than one container element on a page.

2. In the container, have 1 or more rows (```<div class="row"></div>```).

3. In each row have 1 or more columns with widths that add to 12. For example:

    ```<div class="col-md-12"></div>``` or

    ```
    <div class="col-md-6"></div>
    <div class="col-md-6"></div>
```

    The size qualifier ```-md-``` determines the content layout for devices of different sizes (```xs```, ```sm```, ```md```, ```lg```). Whichever one you choose, Bootstrap will handle the layout across devices, just in different ways. See the Bootstrap documentation for further details.

4. Place your content in the columns.

5. Rows can be nested in columns.
