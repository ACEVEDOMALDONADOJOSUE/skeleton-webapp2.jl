# Working with mapping data


Here are some notes about obtaining the mapping data used in the examples.

1. **Obtain the shape files**. The original data consists of shape files, specifically Statistical Area Level 2 data for Australia. These can be downloaded in compressed format from the Australian Bureau of Statistics.
2. **Uncompress the shape file**.
3. **Convert it to GeoJSON and retain only the desired data**.

    - I used the ``gdal`` which translates between various geospatial data formats - it's availabble on all platforms.
    - Then I ran the following command:

```
ogr2ogr -f GeoJSON sa2.json SA2_2011_AUST.shp
```

4. **Filter the data**. The resuling data is a whopping 171MB. To keep things simple I filtered for data pertaining to the state of Victoria in Australia, which is shown in the examples. Here is the command (the clause ``"STE_NAME11 = 'Victoria'"`` does the filtering):

```
ogr2ogr -f GeoJSON -where "STE_NAME11 = 'Victoria'" sa2vic.json SA2_2011_AUST.shp
```

5. **Make it smaller**. The Victorian data is still 28MB of GeoJSON, which is too much to send over the wire, and interactivity would be sluggish. To shrink it down, note that the polygons that define the regions are quite detailed. I used the fabulous free online tool [mapshaper](mapshaper.org) to sacrifice some of this detail and obtain a 1MB GeoJSON file (I used a 3% simplification factor, which obtained a good approximation to the original data, though the approximation becomes poorer as you zoom in). 

6. **Obtain the response variable**. The GeoJSON data describes the spatial regions. The data we are interested in, the response variable, must come from a separate source. For the examples I created a Julia dictionary that maps the GeoJSON feature names to a random number. See below for the Julia code.


## To do

For mapping data, use TopoJSON instead of GeoJSON because it is smaller, often a fraction of the size. GeoJSON can be converted to TopoJSON using the [mapshaper](mapshaper.org) tool or the _topojson_ package available via ``npm`` (feeding the 1MB ``sa2vic.json`` to the _mapshaper_ tool without any simplification yields a 305KB file). If your mapping library requires GeoJSON (e.g., _leaflet_) there are several JavaScript libraries you can use to convert the TopoJSON to GeoJSON on the client side. The TopoJSON plus the conversion library is usually much smaller than the corresponding GeoJSON.


## Appendix: Julia code for obtaining the response variable
```
using JSON

result  = Dict{String, Float64}()              # sa2_name => rand()
vicdata = JSON.parsefile("sa2vic_min.json")    # Import the GeoJSON FeatureCollection
vicdata = vicdata["features"]                  # Array of features, where each feature is a Dict{String, Any}
n = size(vicdata, 1)
for i = 1:n
    feature = vicdata[i]
    sa2_name = feature["properties"]["SA2_NAME11"]
    result[sa2_name] = rand()
end

# Write result to file
outfile = open("response.json", "w")
write(outfile, JSON.json(result))
close(outfile)
```
