# Outline

## Introduction

This page is an outline of documentation for this repository that will eventually be written properly.  It also indicates features that are yet to be implemented.

This repository is intended to:

- Enable people who aren't web developers to learn web development in Julia.
- Provide a set of example web applications written in Julia that can be adapted to various needs.  Most of the examples are geared towards data scientists who have never built a web application before, but they can be adapted to other applications.

Please note:

- __This repo requires Julia 0.4.x__.
- I am learning this stuff as I go. Suggestions from more experienced web developers are welcome, as well as implementations of some missing features.
- There are lots of other features that could be added...feel free to contribute features, bug reports and anything else that may help.

## Quick Start
See [here](crash_course.md) for a one-page introduction to web development.
Then check out the `main_xxx.jl` files in the examples (see `src/apps/example_xxx/main_xxx.jl`):

- Example 1 - Hello World
- Example 2 - Templates
- Example 3 - Serving files
- Example 4 - Insecure sessions
- Example 5 - Secure sessions
- Example 6 - Authorisation


# Web Development in Julia

## Make it Work

- Example 1 - Hello World
	- The basic request/response pattern
		- The client:
			- The client sends an http request to the server. For example, when a web user enters a URL into a browser and presses `Enter`, the browser constructs an http request and sends it to the server hosting the requested resource. Requests can also be created programatically without a browser. See [Requests.jl](https://github.com/JuliaWeb/Requests.jl) for examples.
			- The http request is a language-independent document with a structured format.
		- The server:
			- Incoming:
				- The server receives data in the form of an http request.
				- The request is parsed into a `Request` object as defined in [HttpCommon.jl](https://github.com/JuliaWeb/HttpCommon.jl)
			- Internal:
				- The server passes the `Request` object into a user-defined function that takes the `Request` and returns a `Response` object, also defined in [HttpCommon.jl](https://github.com/JuliaWeb/HttpCommon.jl). This function defines the application.
			- Outgoing:
				- The server writes the `Response` to an http response, which is a language-independent, structured document.
				- The http response is sent to the client.
	- You can see request details in your browser's developer console. In Firefox type ``Crtl+Shift+K``. In Chrome type ``Ctrl+Shift+I`` and select the _Network_ tab.
	- This example returns some text to the client via `Response.data`
- Example 2 - Templates
	- Writing data to a template, then serving the result
	- Returns text as part of a template
	- The returned text is hard coded on the server side...this example can be adapted to serve text and/or data that depends on user input.
- Example 3 - Serving Files
	- Serves entire files
	- A data science example that serves data in templates as per Example 2, as well as static files for formatting (css files) and interactivity (JavaScript files). The result is an application that:
		- Reads data from a csv
		- Runs a linear regression
		- Produces some interactive charts that can be viewed in your browser
	- This example also demonstrates:
		- Running numerical computations in Julia as part of preparing the response
		- The use of GeoJSON data in mapping. See [Working With Mapping Data](working_with_mapping_data.md) for further details.
		- A formulaic approach to structuring web pages using the [Bootstrap](http://getbootstrap.com/javascript) framework developed at Twitter. See [Structuring Web Pages](structuring_web_pages.md) for further details.
	- Note that files served as part of an HTML page, such as CSS and Javascript files, are served in separate requests. See your browser's developer console for details.
- Example 4 - Insecure Sessions
	- A session is a conversation between the server and client; that is, a sequence of request-response cycles in which relevant information about the client is known to the server during each cycle. An example of relevant information might be the contents of a shopping cart. Without sessions, the server doesn't know that two requests from the same client are indeed from the same client, in which case the client would have to re-enter the relevant information with each request.
	- The session implemented in this example introduces:
		- HTML forms, a mechanism that allows the browser to capture data from the user.
		- Cookies, a means of passing data and instructions back and forth between the client and server. Cookies are used to store session information.
		- Ajax, a method for making a non-blocking request for a specific piece of data. This example uses ajax to create a session cookie from the home page without having to reload the whole page.
	- The session implemented in this example is insecure because it can be read, modified and impersonated by an eavesdropper. 


## Make it Secure
- Example 5 - Secure Sessions
	- Sessions with security features that avoid the session being eavesdropped, modified or impersonated.
	- The session implemented in this example introduces:
		- HTTPS, an encrypted form of the HTTP protocol.
		- Authentication, enabling the server to verify the identity of the client and detect message tampering.
	- **Note: The security of this implementation has not been verified by a security professional. See [Security Protocols](https://github.com/JockLawrie/SecureSessions.jl) for details.**
- Example 6 - Authorisation
	- Login: Allowing access to content only if permission is granted.
	- Note the distinction between authorisation and authentication:
		- Authentication involves verifiying a client's identity (see previous example)
		- Once that happens, authorisation determines whether the client has permission to access the requested content.
	- This example also uses URL redirection via http status codes. When a user who is not logged in tries to access content, s/he is redirected to the home page for login.


## Make it Fast

- Minify / Compile
	- Minimise the volume of content being returned.
	- See [Working With Mapping Data](working_with_mapping_data.md) for tips for reducing the size of GeoJSON data.
- Ajax: When a user requests updated data, serve only the updated data...don't reload the whole page.
- Content delivery networks: Many 3rd party libraries, which consist of static files, can be served from other servers that are part of *content delivery networks* (CDNs). This reduces the load on your server and reduces latency (the period between making a request and receiving the response). CDNs have many other uses as well.

## Miscellaneous

- Middleware
- Development frameworks
- Testing
- Deployment
