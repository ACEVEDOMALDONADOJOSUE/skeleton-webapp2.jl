# Contents: Functions not specific to the current app.

using HttpServer


function run(app::Function, port::Integer)
    # Wrapper around HttpServer.run()
    #
    # app: Function that takes a Request and returns a Response.
    server = Server((req, res) -> app(req))    # Create Server instance with server.http.handle being an anonymous function
    HttpServer.run(server, port)               # This is a blocking function
end


################################################################################
### Generic handlers
################################################################################
function notfound!(req, res)
    # Modifies response to not found status
    res.status = 404
    res.data   = "<h1>Resource not found:</h1> <h3>$(req.resource[2:end])</h3> <h3>is not a valid resource.</h3>"
end


function file_response!(req, filename, res)
    # If filename exists populate response with file data, else set response to not found.
    res2 = HttpServer.FileResponse(filename)
    res.status   = res2.status
    res.headers  = res2.headers
    res.data     = res2.data
end


################################################################################
### Other utils
################################################################################
function username_is_valid(username)
    # Returns true if:
    # - length(username) <= 20
    # - username contains none of the forbidden tags defined below.
    result = length(username) <= 20
    if result
        bad_tags = ["<script>", "<link>", "<img>", "<iframe>", "<object>"]
        for tag in bad_tags
            if contains(username, tag)
                result = false
	        break
            end
        end
    end
    result
end


function df_to_dict(df)
    # Represent DataFrame as dict.
    # Often used as the 1st step in converting df to JSON.
    #
    # result = {
    #     "columns" => [name1, name2, ...]
    #     "name1"   => [df11, df21, ...]
    #     "namek"   => [df1k, df2k, ...]
    # }
    result = Dict{ASCIIString, Any}()
    nj     = size(df, 2)
    nms    = names(df)
    result["columns"] = fill("", nj)
    for j = 1:nj
        name_j               = string(nms[j])
        result["columns"][j] = name_j
        result[name_j]       = df[nms[j]]
    end
    result
end
