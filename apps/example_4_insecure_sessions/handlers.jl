# Contents: Functions specific to this app.


function create_session(req, res)
    arr = req.data
    n   = size(arr, 1)
    username = join([Char(arr[i]) for i = 1:n])
    if username_is_valid(username)
        setcookie!(res, "username", username)
    else
        res.status = 400    # Bad request because username is not valid.
    end
end


function home(req, res)
    tpl = open(readall, "templates/home.html")    # Retrieve template
    tpl_data = Dict("home_text" => "Hi, this is the home page. Click the links to see some charts.")    # Set data
    res.data = Mustache.render(tpl, tpl_data)     # Populate the template with data and assign the result to res.data
end


function iris_model(req, res)
    ### Retrieve template
    tpl = open(readall, "templates/onechart.html")    # 1 chart

    ### Generate data
    mdl = lm(PetalWidth ~ PetalLength, df)     # Linear regression model
    chart_spec = JSON.json(
                  Dict("bindto" => "#chart",       # Name of html element that the chart binds to
                       "data"   => Dict(
                           "x"     => "x_data",    # Defines x axis data
                           "json"  => Dict(
                               "x_data"      => df[:PetalLength],
                               "Petal Width" => df[:PetalWidth],
                               "Fitted"      => predict(mdl)),
                           "types" => Dict("Petal Width" => "scatter")),    # Defaults to line chart
                       "axis" => Dict(
                           "x" => Dict(
                               "label" => "Petal Length",
                               "tick"  => Dict("fit" => false)),
                           "y" => Dict(
                               "label" => Dict(
                                   "text"     => "Petal Width",
                                   "position" => "outer-middle")))
                  ))
    tpl_data = Dict("chart_spec" => chart_spec)

    ### Populate the template with data and assign the result to res.data
    res.data = Mustache.render(tpl, tpl_data)
end


function iris_2charts(req, res)
    ### Retrieve template
    tpl = open(readall, "templates/twotabs.html")    # 2 tabs

    ### Generate data
    # Specify 1st chart
    chart1 = JSON.json(
                  Dict("bindto" => "#chart1",      # Name of html element that the chart binds to
                       "data"   => Dict(
                           "xs" => Dict(
                               "setosa"     => "x_setosa",    # Defines x axis data for each series
                               "versicolor" => "x_versicolor",
                               "virginica"  => "x_virginica"),
                           "json" => Dict(
                               "x_setosa"     => df[df[:Species] .== "setosa",     :PetalLength],
                               "x_versicolor" => df[df[:Species] .== "versicolor", :PetalLength],
                               "x_virginica"  => df[df[:Species] .== "virginica",  :PetalLength],
                               "setosa"       => df[df[:Species] .== "setosa",     :PetalWidth],
                               "versicolor"   => df[df[:Species] .== "versicolor", :PetalWidth],
                               "virginica"    => df[df[:Species] .== "virginica",  :PetalWidth]),
                           "type" => "scatter"),
                       "axis" => Dict(
                           "x" => Dict(
                               "label" => "Petal Length",
                               "tick"  => Dict("fit" => false)),
                           "y" => Dict(
                               "label" => Dict(
                                   "text"     => "Petal Width",
                                   "position" => "outer-middle")))
                  ))

    # Specify 2nd chart
    mean_petal_length = by(df, :Species, df -> mean(df[:PetalLength]))
    mean_petal_width  = by(df, :Species, df -> mean(df[:PetalWidth]))
    chart2 = JSON.json(
                  Dict("bindto" => "#chart2",      # Name of html element that the chart binds to
                       "data" => Dict(
                           "json" => Dict(
                               "Mean Petal Length" => mean_petal_length[:x1],
                               "Mean Petal Width"  => mean_petal_width[:x1]),
                           "type" => "bar"),
                       "bar"  => Dict("width" => ("ratio" => 0.5)),
                       "axis" => Dict(
                           "x" => Dict(
                               "type"       => "category",
                               "categories" => mean_petal_length[:Species]))
                  ))

    tpl_data = Dict("chart1" => chart1, "chart2" => chart2)

    ### Populate the template with data and assign the result to res.data
    res.data = Mustache.render(tpl, tpl_data)
end


function map_chloropleth(req, res)
    tpl = open(readall, "templates/map_chloropleth.html")    # Retrieve template

    # Set data
    shapedata     = open(readall, "../../data/geo/sa2vic_min.json")
    response_data = open(readall, "../../data/geo/response.json")
    tpl_data = Dict("shapedata" => shapedata, "response_data" => response_data)

    # Populate the template with data and assign the result to res.data
    res.data = Mustache.render(tpl, tpl_data)
end


function show_table(req, res)
    tpl = open(readall, "templates/show_table.html")    # Retrieve template
    table_data = JSON.json(df_to_dict(df))
    tpl_data = Dict("table_data" => table_data)
    res.data = Mustache.render(tpl, tpl_data)
end
