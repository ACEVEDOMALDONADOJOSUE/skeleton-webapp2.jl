################################################################################
#
# App: example_5_secure_sessions
#
# To run this app:
# - Type:
#     cd path/to/skeleton-webapp.jl/apps/example_6_authorisation
#     julia main_authorisation.jl
# - In your browser go to https://0.0.0.0:8000/home
#
# Notes:
# 1) If you are not on a Unix-like operating system you will to generate the certificate and key files manually.
#
# 2) If you are testing this locally with a browser and run into an error something like this:
#        MbedTLS error code -25088: CIPHER - Input data contains invalid padding and is rejected
#
#    Then you may have:
#    - set a "sessionid" cookie during a session
#    - stopped the server and restarted it before the expiry of the cookie you set.
#      (therefore the newly generated iv, secret_key, etc will not decrypt the old cookie_value)
#
#    To fix this, delete the cookie from your browser.
#
# 3) The SecureSessions package is not registered. To get it, type:
#    cd ~/.julia/v0.4
#    git clone https://github.com/JockLawrie/SecureSessions.jl.git
#
################################################################################


################################################################################
### Includes
################################################################################
using HttpCommon       # Request and Response types plus mime types
using HttpServer       # Http/websockets server
using Mustache         # For populating html templates with data
using DataFrames       # Helps store tabular data in memory
using JSON             # For converting julia dictionaries to JSON data
using GLM              # For statistical regression models
using SecureSessions   # For storing user information securely in a session cookie

include("http_utils.jl")    # Contents: Functions not specific to this app.
include("handlers.jl")      # Contents: Functions     specific to this app.

################################################################################
### Generate a certificate and key if they do not already exist.
################################################################################
rel(p::AbstractString) = joinpath(dirname(@__FILE__), p)
if !isfile("keys/server.crt")
    @unix_only begin
        run(`mkdir -p $(rel("keys"))`)
        run(`openssl req -x509 -nodes -days 365 -newkey rsa:2048 -keyout
        $(rel("keys/server.key")) -out $(rel("keys/server.crt"))`)
    end
end

################################################################################
### Application logic
################################################################################

# Raw data from csv. Could be from database, website, etc.
# In this app the data is the same for several paths so read it from disk just once.
const df = readtable("../../data/iris_data.csv")    
const password_store = Dict{AbstractString, StoredPassword}()    # username => PasswordStore(salt, hash(password))
password_store["Alice"] = StoredPassword("pwd_@l1cE")
password_store["Bob"]   = StoredPassword("pwd_B0b")


# Associate each path with a handler: path => handler
const paths = Dict{ASCIIString, Function}()
paths["/home"]       = home
paths["/model"]      = iris_model
paths["/2charts"]    = iris_2charts
paths["/table"]      = show_table
paths["/map_chloro"] = map_chloropleth


# The app is just a function that takes in a Request and returns a Response.
# The response is initialised at the start of the function and modified later in the function.
# The request is never modified.
function app(req::Request)
    res = Response()                                   # Initialise response
    if ismatch(r"^/static/*", req.resource)            # Serve static file if requested
	filename = string("../..", req.resource)           # Example: "/static/js/bootstrap.min.js" becomes "../../static/js/bootstrap.min.js"
	file_response!(req, res, filename)                 # Either populates res.data with the file contents, or res = notfound.
    elseif haskey(paths, req.resource)                 # else consult paths table for handler
	username = get_session_cookie_data(req, "sessionid")    # Extract username from session cookie; defaults to "".
	if username == "" && req.resource != "/home"            # Redirect to login on home page
	    res.status = 303
	    res.headers["Location"] = "/home"
	else
	    tpl, tpl_data = paths[req.resource](req, res)       # Template and data
	    tpl_data["welcome_text"] = "Welcome $username"      # Add welcome text to template data
	    res.data = Mustache.render(tpl, tpl_data)           # Populate the template with data and assign the result to res.data
	end
    elseif req.resource == "/login"                    # else create session if requested
	# Check whether username and password adhere to the formatting rules specified in SecureSessions
	username_password_are_permissible = false
        data     = JSON.parse(bytestring(req.data))        # Dict("username" => username, "password" => password)
	username = data["username"]
	password = data["password"]
        if username_is_permissible(username)
	    if password_is_permissible(password)
		username_password_are_permissible = true
	    end
	end

	# Compare the password supplied in the request to the stored password
	password_ok = false
	if username_password_are_permissible
	    if haskey(password_store, username)
		if password_is_valid(password, password_store[username])
		    password_ok = true
		end
	    end
	end

	# Create session cookie if username and password satisfy conditions
	if username_password_are_permissible && password_ok    # The 1st condition has already been checked, we include it again for clarity
	    create_secure_session_cookie(username, res, "sessionid")
	else
	    res.status = 400
	end
    elseif req.resource == "/logout"
	res.data = "/home"
	setcookie!(res, "sessionid", utf8(""), Dict("Max-Age" => utf8("0")))    # Session no longer valid
    else                                               # else requested resource not found
	notfound!(req, res)
    end
    res
end


# Define and run server
server = Server((req, res) -> app(req))
server.http.events["listen"] = (saddr) -> println("Running on https://$saddr (Press CTRL+C to quit)")
cert   = MbedTLS.crt_parse_file(rel("keys/server.crt"))
key    = MbedTLS.parse_keyfile(rel("keys/server.key"))
run(server, port = 8000, ssl = (cert, key))    # This is a blocking function
