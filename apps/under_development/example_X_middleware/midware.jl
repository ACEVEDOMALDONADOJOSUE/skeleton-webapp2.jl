################################################################################
### Functionality
################################################################################

function serve_static(req, res)
    # Serve files from static/ folder.
    # This function uses HttpCommon.FileResponse rather than Meddle.FileServer because the latter:
    # - Permits access to any file on the filesystem
    # - Ignores mime types  
    if ismatch(r"/static/*", req.http_req.resource)
        serve_file(req.http_req, res)
        return respond(req, res)
    end
    req, res
end


function handle_valid_requests(req, res)
    if haskey(routes, req.http_req.resource)
        routes[req.http_req.resource](req, res)
        return respond(req, res)
    end
    req, res
end


function bad_request(req, res)
    res.status = 400
    res.data   = "<h1>Bad request:</h1> <h3>$(req.http_req.resource[2:end])</h3> <h3>is not a valid resource.</h3>"
    respond(req, res)
end


################################################################################
### Map names of each midware object to its functionality
################################################################################

ServeStaticFiles    = Midware(serve_static)
HandleValidRequests = Midware(handle_valid_requests)
BadRequest          = Midware(bad_request)
