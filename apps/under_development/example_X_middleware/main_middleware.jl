################################################################################
#
# App: example_4_middleware
#
# To run this app:
# - Type:
#     cd path/to/skeleton-webapp.jl/apps/example_4_middleware
#     julia main_middleware.jl
# - In your browser go to localhost:8000/home
#
################################################################################


################################################################################
### Includes
################################################################################
using HttpCommon    # Defines FileResponse (among other things)
using HttpServer    # Basic http/websockets server
using Mux           # Contains middleware: a sequence functions that each modify the request and response. 
using Mustache      # For populating html templates with requested data
using DataFrames    # For storing data in memory
using JSON          # For converting julia dictionaries to JSON data
using GLM           # For regression models

include("utils.jl")       # Utility functions
include("handlers.jl")    # Contains our handler functions
#include("midware.jl")     # Contains the functions that operate on each request/response pair


################################################################################
### Global variables
################################################################################
const df = readtable("../../data/iris_data.csv")    # Raw data from csv, database, website, etc


################################################################################
### Application logic
################################################################################

# Dispatch request to appropriate handler
@app app = (
#    Mux.defaults,
    page("/home",     req -> home(req))
#    page("/model",      iris_model),
#    page("/2charts",    iris_2charts),
#    page("/table",      show_table),
#    page("/map_chloro", map_chloropleth),
#    Mux.notfound()
)

run(Server(Mux.http_handler(app)), 8000)    # Run synchronously
#serve(app, 8000)                            # Run asynchronously


### EOF
