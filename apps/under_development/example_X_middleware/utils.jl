# Utility functions

function df_to_dict(df)
    # Represent df as dict.
    # Often used as the 1st step in converting df to JSON.
    #
    # result = {
    #     "columns" => [name1, name2, ...]
    #     "name1"   => [df11, df21, ...]
    #     "namek"   => [df1k, df2k, ...]
    # }
    result = Dict{ASCIIString, Any}()
    nj = size(df, 2)
    result["columns"] = fill("", nj)
    nms = names(df)
    for j = 1:nj
        name_j = string(nms[j])
        result["columns"][j] = name_j
        result[name_j] = df[nms[j]]
    end
    result
end
