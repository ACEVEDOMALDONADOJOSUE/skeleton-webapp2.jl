# Contents: Crypto functions

function csrng(numbytes::Integer)
    # Cryptographically secure RNG
    entropy = MbedTLS.Entropy()
    rng     = MbedTLS.CtrDrbg()
    MbedTLS.seed!(rng, entropy)
    rand(rng, numbytes)
end


function encrypt_cbc(ciphertype, secret_key, iv, plaintext)
    # Input:   Plaintext,  Array{UInt8, n1}
    # Output   Ciphertext, Array{UInt8, n2}
    # Example: aescbc("AES256", secret_key, iv, plaintext)
    blocksize = length(secret_key)
    assert(length(iv) == blocksize)
    n1 = size(plaintext, 1)
    nblocks = div(n1, blocksize)
    if rem(n1, blocksize) > 0
        nblocks += 1
    end
    result = zeros(UInt8, nblocks * blocksize)

    # Loop through blocks
    startidx  = 1
    scrambled = iv
    for i = 1:nblocks
        endidx = min(n1, startidx + blocksize - 1)
	block  = view(plaintext, startidx:endidx)
	if i == nblocks && size(block, 1) < blocksize    # Pad block with zeros
	    block = zeros(UInt8, blocksize)
	    for j = startidx:endidx
	        block[j - startidx + 1] = plaintext[j]
	    end
            endidx = startidx + blocksize - 1
        end
	block_xor = scrambled $ block
        block_enc = encrypt(ciphertype, secret_key, block_xor)
	for j = startidx:endidx
	    result[j] = block_enc[j - startidx + 1]
	end

	# Set up next iteration
	if i < nblocks
	    startidx  = endidx + 1
  	    scrambled = block_enc
        end
    end
    result
end


function decrypt_cbc(ciphertype, secret_key, iv, ciphertext)
    # Input   Ciphertext, Array{UInt8, n1}
    # Output: Plaintext,  Array{UInt8, n2}
    # Example: aescbc("AES256", secret_key, iv, plaintext)
    blocksize = length(secret_key)
    assert(length(iv) == blocksize)
    n2 = size(ciphertext, 1)
    nblocks = div(n2, blocksize)
    if rem(n2, blocksize) > 0
        error("length(ciphertext) is not a multiple of block_size.")
    end
    result = zeros(UInt8, nblocks * blocksize)

    # Loop through blocks
    startidx  = 1
    scrambled = iv
    for i = 1:nblocks
        endidx      = min(n2, startidx + blocksize - 1)
	block       = view(ciphertext, startidx:endidx)
        block_dec   = decrypt(ciphertype, secret_key, block)
	block_plain = scrambled $ block_dec
	for j = startidx:endidx
	    result[j] = block_plain[j - startidx + 1]
	end

	# Set up next iteration
	if i < nblocks
	    startidx  = endidx + 1
  	    scrambled = block
        end
    end

    # Truncate result from first occurence of \0
    idx = findfirst(result, '\0')
    if idx > 0
        result = result[1:(idx - 1)]
    end
    result
end


### EOF
