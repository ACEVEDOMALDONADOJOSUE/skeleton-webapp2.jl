################################################################################
#
# App: example_3_servefiles
#
# To run this app:
# - Type:
#     cd path/to/skeleton-webapp.jl/apps/example_3_servefiles
#     julia main_servefiles.jl
# - In your browser go to localhost:8000/home
#
################################################################################


################################################################################
### Includes
################################################################################
using HttpCommon    # Request and Response types plus mime types.
using HttpServer    # Http/websockets server
using Mustache      # For populating html templates with data
using DataFrames    # Helps store tabular data in memory
using JSON          # For converting julia dictionaries to JSON data
using GLM           # For statistical regression models

include("http_utils.jl")    # Contents: Functions not specific to this app.
include("handlers.jl")      # Contents: Functions     specific to this app.


################################################################################
### Application logic
################################################################################

# Raw data from csv. Could be from database, website, etc.
# In this app the data is the same for several paths so read it from disk just once.
const df = readtable("../../data/iris_data.csv")    


# Associate each path with a handler: path => handler
const paths = Dict{String, Function}()
paths["/home"]       = home
paths["/model"]      = iris_model
paths["/2charts"]    = iris_2charts
paths["/table"]      = show_table
paths["/map_chloro"] = map_chloropleth


# The app is just a function that takes in a Request and returns a Response.
# The response is initialised at the start of the function and modified later in the function.
# The request is never modified.
function app(req::Request)
    res = Response()                           # Initialise response
    if ismatch(r"^/static/*", req.resource)    # Serve static file if requested
	filename = string("../..", req.resource)    # Example: "/static/js/bootstrap.min.js" becomes "../../static/js/bootstrap.min.js"
	file_response!(req, filename, res)          # Either populates res.data with the file contents, or res = notfound.
    elseif haskey(paths, req.resource)         # else consult paths table for handler
        paths[req.resource](req, res)
    else                                       # else requested resource not found
	notfound!(req, res)
    end
    res                                        # Return response
end

run(app, 8000)    # This is a blocking function
